source /home/maxking/dotfiles/antigen/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle pip
antigen bundle command-not-found

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting

# Load the theme.
antigen theme steeef

# Tell antigen that you're done.
antigen apply

source /usr/local/bin/virtualenvwrapper.sh
export PATH=$PATH:"/home/maxking/.cabal/bin"

chpwd_auto_ls () {
    ls --color=if-tty --group-directories-first -hF
}

add-zsh-hook chpwd chpwd_auto_ls
eval "$(thefuck --alias)"

export LD_LIBRARY_PATH=/usr/lib/vmware/lib/libglibmm-2.4.so.1/:$LD_LIBRARY_PATH
