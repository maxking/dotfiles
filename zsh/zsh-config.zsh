source /home/maxking/dotfiles/antigen/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle pip
antigen bundle command-not-found

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting

# Load the theme.
antigen theme steeef

# Tell antigen that you're done.
antigen apply

alias reload!='source ~/.zshrc'
alias l="ls -GlAh --color"
alias ls="ls --color=auto -Fh --group-directories-first"
alias latest="ls -lct1"
alias clls="clear;ls -Glah --color"
alias pingg='ping -c 4 Google'
alias grep='grep --colour=auto'
alias diff='colordiff'
alias cd..='cd ..'
alias .='pwd'
alias ..='cd ..'
alias ...='cd ../..'
alias sl=ls
alias copy='rsync -aP'
alias halt='sudo shutdown -h now'
alias reboot='sudo reboot'
alias dt='dmesg | tail'
alias dh='df -h'
alias dm='df -m'
alias x='startx'
alias wv='sudo wvdial'
alias bc='bc -l'
alias update='sudo apt-get update'
alias search='apt-cache search'
alias install='sudo apt-get install --yes'
alias upgrade='sudo apt-get upgrade --yes'
alias ae='sudo $EDITOR /etc/apt/sources.list'
alias aL='dpkg -L'
alias em='emacsclient -nw'
alias sem='sudo emacs -nw'
alias cb='curl -F "clbin=<-" https://clbin'

source /usr/local/bin/virtualenvwrapper.sh
export PATH=$PATH:"/home/maxking/.cabal/bin"

chpwd_auto_ls () {
    ls --color=if-tty --group-directories-first -hF
}

add-zsh-hook chpwd chpwd_auto_ls
eval "$(thefuck --alias)"

export LD_LIBRARY_PATH=/usr/lib/vmware/lib/libglibmm-2.4.so.1/:$LD_LIBRARY_PATH
