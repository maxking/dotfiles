

;;----------
;; Helm
;;----------
;; (require 'helm-config)

;; ;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
;; ;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
;; ;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
;; (global-set-key (kbd "C-c h") 'helm-command-prefix)
;; (global-unset-key (kbd "C-x c"))

;; (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
;; (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action)
;; (define-key helm-map (kbd "C-z")  'helm-select-action)

;; (when (executable-find "curl")
;;   (setq helm-google-suggest-use-curl-p t))

;; (setq helm-quick-update                     t
;;       helm-split-window-in-side-p           t
;;       helm-buffers-fuzzy-matching           t
;;       helm-move-to-line-cycle-in-source     t
;;       helm-ff-search-library-in-sexp        t
;;       helm-scroll-amount                    8
;;       helm-ff-file-name-history-use-recentf t)

;; (helm-mode 1)



;;(projectile-global-mode)




;;-----
;; Cask
;;-----
;; (require 'cask)
;; (cask-initialize)
;; (require 'pallet)



;;------------------------
;; Python Specific things
;;-----------------------
;; (require 'flymake-python-pyflakes)
;; (add-hook 'python-mode-hook 'flymake-python-pyflakes-load)
;; (flymke-mode &optional 1)





;;Remove the whitespace mode in erlang-major mode


;;;; grant fuzzy-searching superpowers to isearch
;; (require 'fuzzy)
;; (turn-on-fuzzy-isearch)




;; elfeed settings


;; smart-mode-line
;;(require 'smart-mode-line)
;;(require 'smart-mode-line-powerline-theme)
;;(sml/setup)
;;(sml/apply-theme 'powerline)


;; Auto-update packages
;; (require 'auto-package-update)
;; (auto-package-update-maybe)
;; (setq auto-package-update-interval 10)

;; ;; Setup God mode
;; (require 'god-mode)
;; (global-set-key (kbd "<escape>") 'god-mode-all)
;; (setq god-exempt-major-modes nil)
;; (setq god-exempt-predicates nil)


(provide 'init-testing)
