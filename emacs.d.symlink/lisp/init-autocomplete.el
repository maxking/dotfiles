(require-package 'auto-complete)
(require 'auto-complete-config)


(global-auto-complete-mode t)
(setq-default ac-expand-on-auto-complete nil
			  ac-auto-start nil
			  ac-dwim nil)
			  ;; To get pop-ups with docs even if a word is uniquely completed
(ac-config-default)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")

(setq tab-always-indent 'complete)  ;; use 't when auto-complete is disabled
(add-to-list 'completion-styles 'initials t)


(provide 'init-autocomplete)
