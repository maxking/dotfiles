;;------------------------
;; General Customizations
;;------------------------
(setq-default
 inhibit-start-message t
 inhibit-splash-screen t
 initial-scratch-message nil
 font-lock-maximum-decoration t
 ident-tabs-mode t
 require-final-newline t
 resize-minubuffer-frame t
 column-number-mode t
 blink-matching-paren t
 blick-matching-delay 0.25
 initial-major-mode 'text-mode
 fill-column 80
 tab-width 4
 case-fold-search t
 tooltip-delay 1.5
 visible-bell t)

;; (require-package 'undo-tree)
;; (global-undo-tree-mode)
;; (diminish 'undo-tree-mode)


;;----------------------------------------------------------------------------
;; Page break lines
;;----------------------------------------------------------------------------
;; (require-package 'page-break-lines)
;; (global-page-break-lines-mode)
;; (diminish 'page-break-lines-mode)


(setq backup-directory-alist `(("." . "~/.saves")))


(require-package 'highlight-escape-sequences)
(hes-mode)


;;-----------------------
;; Final newline hadnling
;;-----------------------
(setq require-final-newline t)
(setq next-line-extends-end-of-buffer nil)
(setq next-line-add-newlines nil)

;; General mode loading
(show-paren-mode t)

(setq stack-trace-on-error t)
(global-auto-revert-mode t)

;; y-or-n-p mode
(defalias 'yes-or-no-p 'y-or-n-p)


;; use the X windows system clipboard
(setq x-select-enable-clipboard 1)

;; default line spacing, original value was nil
(setq-default line-spacing 1)

;; delete selections with backspace and overwrite them by starting to type
(delete-selection-mode t)

;; track recently-opened files
(recentf-mode 1)
(setq recentf-max-menu-items 100)

;; always follow symlinks to files under version control without asking
(setq vc-follow-symlinks t)

;; join the current line with the next one
(global-set-key (kbd "M-j")
				(lambda ()
                  (interactive)
                  (join-line -1)))
;;-----------
;; HL mode
;;-----------
(global-hl-line-mode 1)
(set-face-background 'hl-line "gray20")

;; ---------
;; html-mode
;; ---------
(add-hook 'html-mode-hook
		  (lambda ()
			(setq indent-tabs-mode nil)))

;;---------------------
;; linum mode
;;---------------------
(global-linum-mode 1)
(setq linum-format "%d")


;;-----------------
;; Proejctile Mode
;; ----------------
(require-package 'projectile)
;;(add-hook 'prog-mode-hook 'projectile-mode)

;;-------------------
;; Electric Pair Mode
;;-------------------
(electric-pair-mode 1)

(put 'narrow-to-region 'disabled nil)


(defun untabify-buffer ()
  (interactive)
  (untabify (point-min) (point-max)))

(defun indent-buffer ()
  (interactive)
  (indent-region (point-min) (point-max)))

(defun cleanup-buffer ()
  "Perform a bunch of operations on the whitespace content of a buffer."
  (interactive)
  (indent-buffer)
  (untabify-buffer)
  (delete-trailing-whitespace))

(defun cleanup-region (beg end)
  "Remove tmux artifacts from region."
  (interactive "r")
  (dolist (re '("\\\\│\·*\n" "\W*│\·*"))
    (replace-regexp re "" nil beg end)))

(global-set-key (kbd "C-x M-t") 'cleanup-region)
(global-set-key (kbd "C-c n") 'cleanup-buffer)



(provide 'init-edit-utils)
