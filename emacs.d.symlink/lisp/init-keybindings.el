(global-set-key (kbd "M-x") 'maxking/smex)

(global-set-key (kbd "C-<return>") 'other-window)
(global-set-key [(control c) (m)] 'man)
(global-set-key (kbd "C-c C-r") 'revert-buffer)
(define-key global-map [?\s-l] 'goto-line)
(define-key global-map [?\s-g] 'magit-status)
(global-set-key (kbd "C-c C-f") 'maybe-projectile-find-file)
(global-set-key (kbd "C-c r") 'eval-buffer)

(global-set-key [home] 'smart-beginning-of-line)
(global-set-key (kbd "C-a") 'smart-beginning-of-line)


(global-set-key (kbd "C-c C-r") 'comment-or-uncomment-region-or-line)

;; [(control shift backspace)]
(global-set-key [remap kill-whole-line] 'smart-kill-whole-line)
(global-set-key [remap goto-line] 'goto-line-with-feedback)


;;ecb
(global-set-key (kbd "C-x C-;") 'ecb-activate)
(global-set-key (kbd "C-x C-'") 'ecb-deactivate)
(global-set-key (kbd "C-;") 'ecb-show-ecb-windows)
(global-set-key (kbd "C-'") 'ecb-hide-ecb-windows)


(define-key global-map [?\s-i] 'find-user-init-file)


(define-key global-map [?\s-r] 'find-todo-file)



(global-set-key (kbd "C-c g") 'google)



(global-set-key (kbd "M-g") 'goto-line)


(global-set-key (kbd "<f5>") 'flymake-goto-prev-error)
(global-set-key (kbd "<f6>") 'flymake-goto-next-error)


(require-package 'easy-kill)
(global-set-key [remap kill-ring-save] 'easy-kill)

;; Copy the current word into kill ring
(global-set-key (kbd "C-c w")         (quote copy-word))

(provide 'init-keybindings)
