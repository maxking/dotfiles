;; Unbind C-z, No need to suspend
(when window-system
  (global-unset-key [(control z)]))

;;------------------------------------------------
;; Remove toolbar, menubar, scrollbar and tooltips
;;------------------------------------------------
(tool-bar-mode -1)
(menu-bar-mode -1)
(tooltip-mode -1)
(set-scroll-bar-mode 'nil)

;; Display settings
(add-to-list 'default-frame-alist '(width  . 89))
(add-to-list 'default-frame-alist '(height . 40))
(add-to-list 'default-frame-alist '(font . "Inconsolata-11"))


(provide 'init-windows)
