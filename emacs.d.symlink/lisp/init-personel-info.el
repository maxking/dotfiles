;; Set Personal Information
(setq user-full-name "Abhilash Raj"
	  user-mail-address "raj.abhilash1@gmail.com")

;;-------------------
;; Set Proxy in emacs
;;-------------------
;; Set proxy for my college
;; (setq url-proxy-services
;; 	  '(("no_proxy" . "^\\(localhost\\|10.*\\)")
;; 		("http" . "10.3.100.207:8080")
;; 		("https" . "10.3.100.207:8080")))

(provide 'init-personel-info)
