(global-set-key (kbd "C-x w") 'elfeed)
(setq elfeed-feeds
	  (quote
	   (
		;; "http://eli.thegreenplace.net/feeds/python-internals.atom.xml"
		;; "http://eli.thegreenplace.net/feeds/python.atom.xml"
		;; "http://emacsredux.com/atom.xml"
		;; ; ("http://oremacs.com/atom.xml" emacs)
		;; "http://planet.emacsen.org/atom.xml"
		;; "http://www.masteringemacs.org/feed/"
		;; "http://endlessparentheses.com/atom.xml"
		;; "http://www.lunaryorn.com/feed.atom"
		;; "https://github.com/blog/engineering.atom"
		;; "http://michaelrbernste.in/feed.xml"
		;; "http://www.howardism.org/index.xml"
		;; "http://lambda-the-ultimate.org/rss.xml"
		;; "http://feeds.feedburner.com/codinghorror/"
		;; ("http://prany.github.io/rss.xml" gsoc15)
		;; ("https://towardsgsoc15.wordpress.com/feed/" gsoc15)
		;; ("http://black-perl.me/feed.xml" gsoc15))
		("https://gitlab.com/mailman/mailman.atom?private_token=cQofyCySzpjbw8474fgy" mailman))))


;; Update the feeds every 6 hours.
;;(run-at-time "360 min" :repeat #'elfeed-update)

(provide 'init-elfeed)
