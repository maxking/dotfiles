;;-----
;; smex
;;-----

(require-package 'smex)

(defun maxking/smex ()
  (interactive)
  (condition-case description
      (progn
	(smex-initialize)
	(global-set-key (kbd "M-x") 'smex)
	(global-set-key (kbd "M-X") 'smex-major-mode-commands)
	(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)
	(smex))
    (error (execute-extended-command))))

(provide 'init-smex)
