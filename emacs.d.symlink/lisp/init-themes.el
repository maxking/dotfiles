;; Customizations related to themes

(setq-default display-battery-mode t)
(set-default-font "Inconsolata-13")
(setq framemove-hook-into-windmove t
	  whitespace-style '(face empty lines-tail trailing))
(global-whitespace-mode t)
(mouse-wheel-mode t)
(line-number-mode 1)

(require-package 'zenburn-theme)
(if window-system
	(load-theme 'zenburn t)
  (load-theme 'wombat t))

(global-font-lock-mode 1)

(provide 'init-themes)
