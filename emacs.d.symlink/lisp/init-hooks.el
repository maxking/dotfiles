(require-package 'whitespace)

(add-hook 'erlang-mode-hook
		  (function (lambda ()
					  (global-whitespace-mode 0))))


;; Only use tab-width of 2 for certain modes.
(mapc (lambda (hook)
        (add-hook hook (lambda ()
                         (setq-default tab-width 2))))
      '(js2-mode-hook
        js-mode-hook
        css-mode-hook
        less-css-mode-hook
		))

;; ---------------
;; emacs-lisp-mode
;; ---------------
(add-hook 'emacs-lisp-mode-hook (lambda () (eldoc-mode t)))



;;-------------
;; Text mode
;;-------------
(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'prog-mode-hook 'flyspell-prog-mode)


(add-hook 'find-file-hook
		  (lambda()
			(highlight-phrase "\\(BUG\\|FIXME\\|TODO\\|NOTE\\):")))


(add-hook 'prog-mode-hook 'projectile-mode)


(provide 'init-hooks)
