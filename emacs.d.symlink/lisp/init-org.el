;; Add my personal todo files to org-agenda
(setq org-agenda-files (list "~/Dropbox/Todos/Todo.org"
							 "Dropbox/Todos/MS.org"))

(add-hook 'org-mode-hook
          (lambda ()
            (flyspell-mode)))
(add-hook 'org-mode-hook
          (lambda ()
            (writegood-mode)))

(setq org-agenda-show-log t
      org-agenda-todo-ignore-scheduled t
      org-agenda-todo-ignore-deadlines t)

(provide 'init-org)
