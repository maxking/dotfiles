(require-package 'ido)
(require-package 'ido-vertical-mode)


;;---------
;; Ido mode
;;---------
(ido-mode 1)
(ido-everywhere 1)
(ido-vertical-mode t)

(setq
 ido-ignore-buffers '("\\` " "^\*Mess" "^\*Back" "^\*Ido")
 ido-case-fold t
 ido-use-filename-at-point nil
 ido-use-url-at-point nil
 ido-default-file-method 'selected-window
 ido-default-buffer-method 'selected-window
 ido-max-directory-size nil
 ido-enable-flex-matching t
 ido-auto-merge-work-directories-length -1)

(setq ido-vertical-define-keys 'C-n-C-p-up-down-left-right)


(provide 'init-ido)
