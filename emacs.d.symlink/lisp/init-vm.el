;; This requires vm mua instaled on the system.
(require 'vm-autloads)

(setq vm-primary-inbox "~/.email/inbox.mbox")

;; (setq vm-spool-files `((,vm-primary-inbox
;; 						"imap:imap.gmail.com:993:*:login:raj.abhilash1@gmail.com:*"
;; 						,vm-crash-box))

(add-to-list 'auto-mode-alist '("\\.mbox$" . vm-mode))
(setq mail-user-agent 'vm-user-agent)


(provide 'init-vm)
