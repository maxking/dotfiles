;;------------------------
;; ECB
;;-----------------------
(require 'ecb)
(require 'ecb-autoloads)
(setq ecb-layout-name "left14")
(setq ecb-show-sources-in-directories-buffer 'always)

(provide 'init-ecb)
