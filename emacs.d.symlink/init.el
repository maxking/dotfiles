;;; init -- maxking's emacs initialization file
;; -*- mode: emacs-lisp -*-

;;; Commentary:

;;; Code:
;; Autostart  a server when not running

(require 'server)
(unless (server-running-p)
  (server-start))

;; Add .emacs.d/lisp/ to load path
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(add-to-list 'load-path (expand-file-name "elpa" user-emacs-directory))
(let ((default-directory "~/.emacs.d/elpa/"))
  (normal-top-level-add-subdirs-to-load-path))

(require 'init-personel-info)

;; Variables configured via interactive `customize` command
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

(require 'init-org)
(require 'init-utils)
(require 'init-elpa)
(require 'init-benchmarking) ;; Measure startup time
(require 'init-smex)
(require 'init-paradox)
(require 'init-themes)
(require 'init-flycheck)
(require 'init-git)
(require 'init-elfeed)
(require 'init-flyspell)
(require 'init-autocomplete)
(require 'init-charset)
(require 'init-css)
(require 'init-edit-utils)
(require 'init-hooks)
(require 'init-ido)
(require 'init-windows)
(require 'init-python-mode)
(require 'init-keybindings)
(require 'init-sml)
(require 'init-erc)
(require 'init-c)
(require 'init-haskell)
;;(require 'init-linux)
(require 'init-vm)

;; Set default Browser
(setq browse-url-browser-function 'browse-url-generic
	 browse-url-generic-program "google-chrome")

(provide 'init)
;;; init ends here
